function updateRecord(id){
// alert(title+" "+filename+" "+id);
	var title = $('#btnEdit'+id).data("title");
	var filename = $('#btnEdit'+id).data("filename");

	$("#myModal").css("display","block");
	$('#title').val(title);

 	$('#myModal').data("title",title);
 	$('#myModal').data("id",id);
	$('#myModal').data("image",filename);
 
}
function deleteRecord(id){
	Swal.fire({
	  title: 'Are you sure?',
	  text: "You won't be able to revert this!",
	  icon: 'warning',
	  showCancelButton: true,
	  confirmButtonColor: '#3085d6',
	  cancelButtonColor: '#d33',
	  confirmButtonText: 'Yes, delete it!'
	}).then((result) => {
	  if (result.isConfirmed) {
			$.ajax({
				url: "dbcon.php",
				type: "post",
				data: {act:"delete",id} ,
				success: function (response) {
					Swal.fire('Deleted!','Record was successfully deleted.','success');
					 loadData("fetch");
				},
				error: function(jqXHR, textStatus, errorThrown) {
					   console.log(textStatus, errorThrown);
				}
			});
	   
	  }
	});
}
function loadData(act){
	   $.ajax({
			url: "dbcon.php",
			type: "post",
			data: {act} ,
			success: function (response) {
				var tbody = "";
				$.each(JSON.parse(response), function(i, item) {
					tbody+="<tr><td>"+item.title+"</td><td><img src='assets/upload/"+item.thumbnail+"' style='width:100px'/></td><td>"+item.filename+"</td><td>"+item.dateAdded+"</td><td><a href='#' class='btn-gradient cyan mini' data-title='"+item.title+"' data-filename='"+item.filename+"' id='btnEdit"+i+"' onclick=updateRecord("+i+")>Edit</a><a href='#' class='btn-gradient red mini' onclick='deleteRecord("+i+")'>Delete</a></td></tr>";
				});
				$(".tbl-style>tbody").html(tbody);
			},
			error: function(jqXHR, textStatus, errorThrown) {
				   console.log(textStatus, errorThrown);
			}
		});
}

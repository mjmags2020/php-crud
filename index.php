<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
<link rel="stylesheet" type="text/css" href="assets/css/button.css">
<link rel="stylesheet" type="text/css" href="assets/css/modal.css">
<link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">
<link rel="stylesheet" type="text/css" href="assets/css/sweetalert2.css">
  
<script type="text/javascript" src="assets/js/jquery-3.5.1.min.js"></script>	 
<script type="text/javascript" charset="utf8" src="assets/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="assets/js/scripts.js"></script>
<script type="text/javascript" charset="utf8" src="assets/js/sweetalert2.all.js"></script>
<script>
function showModal(){
	$("#myModal").css("display","block");
	$('#title').val("");
	$('#file').val("");
	$('#myModal').data("image","");
	$('#myModal').data("title","");
	$('#myModal').data("id","0");

}
	function closeModal(){
		$("#myModal").css("display","none");
	}

	$(document).ready( function () {
		loadData("fetch");
		$('.tbl-style').DataTable({
		  "ordering": false,
		  "searching": false
		});

		$('#btnSubmit').click(function(){
				
                var fd = new FormData();

                var files = $('#file')[0].files;
                var title = $('#title').val();

                if(title.length > 0 ){
						var action = ($('#myModal').data("id").toString()=="0") ? "save" : "edit";
   						
						fd.append('file',files[0]);
						fd.append('act',action);
						fd.append('title',title);
						fd.append('id',$('#myModal').data("id"));

						$.ajax({
							url:'dbcon.php',
							type:'post',
							data:fd,
							contentType: false,
							processData: false,
							success:function(response){
								if(response != 0){
									$("#img").attr("src",response);
									loadData("fetch");
									closeModal();
									$('#title').val("");
									$('#file').val("");
 								}else{
									alert('Image file type is required.');
								}
							}
						});
					 
				}else{
                    alert("Title is required");
                }
		});
	});
</script>
</head>
<body>
	<div class="main">
		<a href="#" class="btn-gradient cyan mini" onclick="showModal()">+ Add</a>

		<table class="tbl-style display">
			<thead>
				<tr>
					<th>Title</th>
					<th>Thumbnail</th>
					<th>Filename</th>
					<th>Date added</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
	<div id="myModal" class="modal" data-title="" data-image="" data-id="0">
	  <div class="modal-content">
		<span class="close" onclick="closeModal()">&times;</span>
				<br>	
				<br>	
			<form method="post" action="" enctype="multipart/form-data" id="myform">
				<div class="container">
						<div class="row">
						  <div class="col-25">
							<label for="fname">Title</label>
						  </div>
						  <div class="col-75">
							<input type="text" id="title" name="title" placeholder="Please state the title ..">
						  </div>
						</div>
						<div class="row">
						  <div class="col-25">
							<label for="fname">Image</label>
						  </div>
						  <div class="col-75">
							  <input type="file" id="file" name="filename">

						  </div>
						</div>
					<div class="row" id="modal-footer">
						  <input type="button" value="Submit" id="btnSubmit" class="btn-gradient cyan mini">
	 
					</div>
				</div>
			</form>
	  </div>
	</div>
</body>
</html>